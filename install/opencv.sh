echo "------------- Installing required packages -------------"
sudo apt-get install build-essential
sudo apt-get install cmake git libgtk2.0-dev pkg-config libavcodec-dev libavformat-dev libswscale-dev
sudo apt-get install python-dev python-numpy libtbb2 libtbb-dev libjpeg-dev libpng-dev libtiff-dev libjasper-dev libdc1394-22-dev
echo "------------- Downloading OpenCV 2.4.13.5 -------------"
sudo apt-get install wget
wget -O opencv-2.4.13.5.zip https://github.com/opencv/opencv/archive/2.4.13.5.zip
echo "------------- Installing OpenCV 2.4.13.5 -------------"
unzip opencv-2.4.13.5.zip
cd opencv-2.4.13.5
mkdir release
cd release
cmake -D CMAKE_BUILD_TYPE=RELEASE -D CMAKE_INSTALL_PREFIX=/usr/local ..
make -j7
sudo make install
sudo ldconfig -v
