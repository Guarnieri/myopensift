#CC	= gcc
#CFLAGS	+= -O3
BIN_DIR	= ./bin
SRC_DIR	= ./src
INC_DIR	= ./include
LIB_DIR	= ./lib
BIN	= siftfeat match dspfeat match_num

all: clean $(BIN) libopensift.a

debug: clean
	make CFLAGS='-DDEBUG -DTIME -g' all

time: clean
	make CFLAGS='-DTIME -g' all

libopensift.a:
	make -C $(SRC_DIR) $@

$(BIN):
	make -C $(SRC_DIR) $@

clean:
	make -C $(SRC_DIR) $@;	\
	make -C $(INC_DIR) $@;	\
	rm -f $(LIB_DIR)/*
	rm -f $(BIN_DIR)/*

.PHONY: clean libopensift.a
